install: pack build
	@python setup.py install

# reinstall:
# 	@pip install -i https://pypi.antfin-inc.com/simple py-bigdate-util --upgrade

clean:
	@rm -fr dist/*

pack: clean
	@python setup.py clean
	@python setup.py bdist_wheel
	@python setup.py sdist

build:
	@rm -fr build/*
	@python setup.py build

test:
	@PYTHONPATH=. python test/main.py

check:
	@twine check dist/*.tar.gz dist/*.whl

publish: pack check
	@twine upload -r pypi dist/*.tar.gz dist/*.whl
	@echo '新包已发布，更新包命令： pip install py-bigdate-util --upgrade'

.PHONY: install pack build publish test
