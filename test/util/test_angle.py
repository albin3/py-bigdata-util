#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import unittest
from bigdata_util.util.angle import angle_with_north, min_diff_angle


class AngleTest(unittest.TestCase):

    def test_angle_with_north(self):
        self.assertEqual(angle_with_north([0, 0], [0, 1], False), 0.0)
        self.assertEqual(angle_with_north([0, 0], [1, 1], False), 45.0)
        self.assertEqual(angle_with_north([0, 0], [1, 0], False), 90.0)
        self.assertEqual(angle_with_north([0, 0], [1, -1], False), 135.0)
        self.assertEqual(angle_with_north([0, 0], [0, -1], False), 180.0)
        self.assertEqual(angle_with_north([0, 0], [-1, -1], False), 225.0)
        self.assertEqual(angle_with_north([0, 0], [-1, 0], False), 270.0)
        self.assertEqual(angle_with_north([0, 0], [-1, 1], False), 315.0)
        pass

    def test_min_diff_angle(self):
        self.assertEqual(min_diff_angle(10, 100), 90)
        self.assertEqual(min_diff_angle(10, 350), 20)
        self.assertEqual(min_diff_angle(0, 100), 100)
        self.assertEqual(min_diff_angle(0, 180), 180)
        self.assertEqual(min_diff_angle(1, 180), 179)
        pass


if __name__ == '__main__':
    unittest.main()
    pass
