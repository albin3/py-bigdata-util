#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import unittest
import functools
import datetime
from bigdata_util.util import get_compare_by_key_func, get_reduce_list_to_map_func, get_week_last_day, get_n_days_ago, get_date_range


class UtilTest(unittest.TestCase):
    list_data = [
            {'key': 'a', 'value': 3},
            {'key': 'b', 'value': 1},
            {'key': 'c', 'value': 2},
    ]

    def test_get_compare_by_key_func(self):
        sorted_list = sorted(UtilTest.list_data, key=functools.cmp_to_key(get_compare_by_key_func('value')))
        self.assertEqual(','.join(list(map(lambda x: x['key'], sorted_list))), 'b,c,a')
        pass

    def test_get_reduce_list_to_map_func(self):
        map_data = functools.reduce(get_reduce_list_to_map_func('key'), UtilTest.list_data, {})
        for meta in UtilTest.list_data:
            self.assertEqual(meta, map_data[meta['key']])
        pass

    def test_get_week_last_day(self):
        self.assertEqual(get_week_last_day('20190505'), '20190505')
        self.assertEqual(get_week_last_day('20190501'), '20190505')
        self.assertEqual(get_week_last_day('20190503'), '20190505')
        pass

    def test_get_n_days_ago(self):
        self.assertEqual(get_n_days_ago('20190505', 1), '20190504')
        self.assertEqual(get_n_days_ago('20190505', 0), '20190505')
        self.assertEqual(get_n_days_ago('20190505', -1), '20190506')
        pass

    def test_get_date_range(self):
        self.assertEqual(len(get_date_range('20220101', '20220301')), 60)
        self.assertEqual(len(get_date_range('20220101', '20220101')), 1)
        try:
            get_date_range('29001111', '19991111')
        except Exception as e:
            self.assertEqual(str(e), '开始时间大于结束时间')
            pass
        try:
            get_date_range('19001111', '19991111')
        except Exception as e:
            self.assertEqual(str(e), 'strict 模式下，时间范围不能超过1000天，可以设置 strict=False 突破限制！')
            pass
        pass

    def test_get_time_range(self):
        self.assertEqual(len(get_date_range(
            '2022-06-19 00:00:00',
            '2022-07-01 00:00:00',
            date_format='%Y-%m-%d %H:%M:%S',
            delta_time=datetime.timedelta(minutes=5),
            contains_end=False
        )), 24 * 60 / 5 * 12)
        pass


if __name__ == '__main__':
    unittest.main()
    pass
