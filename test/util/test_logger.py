#!/usr/bin/env python3
# -*- coding=utf-8 -*-
import unittest

from bigdata_util.util import get_logger


class ConfigTest(unittest.TestCase):
    def test_config(self):
        a = get_logger('test_logger_name_aaa')
        a.info('loggertest: aaa')
        b = get_logger('test_logger_name_bbb')
        b.info('loggertest: bbb')
        pass

