#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import unittest


class TestMain:

    @staticmethod
    def run():
        test_dir = './test'
        discover = unittest.defaultTestLoader.discover(test_dir, pattern='test*.py')
        runner = unittest.TextTestRunner(verbosity=2)
        runner.run(discover)
        pass


if __name__ == '__main__':
    TestMain.run()
