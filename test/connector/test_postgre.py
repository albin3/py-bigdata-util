#!/usr/bin/env python3
# -*- coding=utf-8 -*-

import unittest
from bigdata_util.util.base_config import BaseConfig
from bigdata_util.connector import MaxcomputeConnector, PostgreConnector
from bigdata_util.util import get_absolute_path


class PostgreTest(unittest.TestCase):

    def test_save_data_to_file(self):
        table_name = 'test_save_data_to_file'
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))

        # 1
        postgre = PostgreConnector(cfg.get('connector.postgres_uri'))
        postgre.drop_table(table_name)
        postgre.execute_sql(f'''
            create table {table_name} (
                a varchar
            )
        ''')
        with open('test/test.log', 'wb') as writer:
            postgre.save_data(table_name, [
                {'a': '1'},
                {'a': '2'}
            ], show_log=True, batch_size=1, file_writer=writer)
        data_list = postgre.run_sql_return_plain_json(f'select * from {table_name}')
        self.assertEqual(0, len(data_list))
        postgre.run_sql_in_file('test/test.log')
        data_list = postgre.run_sql_return_plain_json(f'select * from {table_name}')
        self.assertEqual(2, len(data_list))
        postgre.drop_table(table_name)

        # 2
        from bigdata_util.connector.postgre_async import PostgreAsyncConnector
        postgre = PostgreAsyncConnector(cfg.get('connector.postgres_uri'))
        postgre.drop_table(table_name)
        postgre.execute_sql(f'''
            create table {table_name} (
                a varchar
            )
        ''')
        with open('test/test.log', 'wb') as writer:
            postgre.save_data(table_name, [
                {'a': '1'},
                {'a': '2'}
            ], show_log=True, batch_size=1, file_writer=writer)
        data_list = postgre.run_sql_return_plain_json(f'select * from {table_name}')
        self.assertEqual(0, len(data_list))
        postgre.run_sql_in_file('test/test.log')
        data_list = postgre.run_sql_return_plain_json(f'select * from {table_name}')
        self.assertEqual(2, len(data_list))
        postgre.drop_table(table_name)
        pass

    def test_create_table(self):
        table_name = 'test_partitioned_table'

        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        postgre = PostgreConnector(cfg.get('connector.postgres_uri'))
        result = postgre.exist_table(table_name)
        postgre.create_table([
            {
                'column_name': 'a',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True,
                'is_partition': True
            },
            {
                'column_name': 'b',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            },
            {
                'column_name': 'c',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            },
            {
                'column_name': 'd',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            }
        ], table_name)
        result = postgre.exist_table(table_name)
        postgre.description(table_name)
        postgre.drop_table(table_name)

    def test_postgre_read_write(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        # odps_ins = MaxcomputeConnector(cfg.get('connector.odps'))
        postgre = PostgreConnector(cfg.get('connector.postgres_uri'))
        l = list(map(lambda x: {"a": str(x), "lnglat_seq": ""}, list(range(0, 10))))
        postgre.save_geometry_table('test_case_dual', l)
        postgre.run_sql_return_plain_json('''
            select * from test_case_dual
        ''')
        postgre.drop_table('test_case_dual')
        pass

    def test_run_in_file(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        postgre = PostgreConnector(cfg.get('connector.postgres'))
        postgre.run_sql_in_file(get_absolute_path(__file__, './test_sql_in_file.sql'), {
            'test_param': 1
        })
        pass

    def test_create_pk_table_and_update_on_conflict(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        postgre = PostgreConnector(cfg.get('connector.postgres'))
        table_name = 'algtmp_test_create_pk_and_update_on_conflict'
        postgre.drop_table(table_name)
        postgre.execute_without_result(f'''
            create table if not exists {table_name} (
                a varchar,
                b varchar,
                c float,
                d varchar,
                PRIMARY KEY (a, b)
            )
        ''')
        postgre.save_data(table_name, [
            {'a': 'a1', 'b': 'b1', 'c': 123, 'd': 'd1'}
        ])
        postgre.save_data(table_name, [
            {'a': 'a1', 'b': 'b1', 'c': 234, 'd': 'd3'},
            {'a': 'a2', 'b': 'b1', 'c': 345, 'd': 'd2'},
        ])
        pass

    def test_create_pk_table_and_update_on_conflict_with_full_key(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        postgre = PostgreConnector(cfg.get('connector.postgres'))
        table_name = 'algtmp_test_create_pk_and_update_on_conflict_full_key'
        postgre.drop_table(table_name)
        postgre.execute_without_result(f'''
            create table if not exists {table_name} (
                a varchar,
                b varchar,
                c varchar,
                d varchar,
                PRIMARY KEY (a, b, c, d)
            )
        ''')
        postgre.save_data(table_name, [
            {'a': 'a1', 'b': 'b1', 'c': 'c2', 'd': 'd1'}
        ])
        postgre.save_data(table_name, [
            {'a': 'a1', 'b': 'b1', 'c': 'c2', 'd': 'd1'},
            {'a': 'a2', 'b': 'b1', 'c': 'c1', 'd': 'd2'},
        ])
        pass

    def test_libpq(self):
        config = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        pg_conn_info = config.get('connector.postgres_uri')
        postgre = PostgreConnector(pg_conn_info)

        table_name = 'algtmp_test_create_pk_and_update_on_conflict_full_key'
        postgre.drop_table(table_name)
        postgre.create_table([
            {
                'column_name': 'a',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            },
            {
                'column_name': 'b',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            },
            {
                'column_name': 'c',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            },
            {
                'column_name': 'd',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            }
        ], table_name)
        # postgre.execute_without_result(f'''
        #     create table if not exists {table_name} (
        #         a varchar,
        #         b varchar,
        #         c varchar,
        #         d varchar,
        #         PRIMARY KEY (a, b, c, d)
        #     )
        # ''')
        postgre.save_data(table_name, [
            {'a': 'a1', 'b': 'b1', 'c': 'c2', 'd': 'd1'}
        ])
        postgre.save_data(table_name, [
            {'a': 'a1', 'b': 'b1', 'c': 'c2', 'd': 'd1'},
            {'a': 'a2', 'b': 'b1', 'c': 'c1', 'd': 'd2'},
        ])
        pass

    def test_proxy_network(self):
        config = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        pg_conn_info = config.get('connector.proxy_pg_conn')
        socks_url = config.get('connector.proxy_pg_socks')

        from bigdata_util.connector.postgre_async import PostgreAsyncConnector
        from bigdata_util.connector.postgre import PostgreConnector
        conn_proxy = PostgreAsyncConnector(pg_conn_info, socks_url)
        conn_normal = PostgreConnector(config.get('connector.postgres_uri'))

        table_name = 'algtmp_py_bigdata_util_test_proxy'
        conn_normal.drop_table(table_name)
        conn_proxy.drop_table(table_name)
        conn_normal.execute_without_result(f'''
            create table if not exists {table_name} (
                a varchar,
                b text,
                c varchar,
                d varchar,
                PRIMARY KEY (a, b)
            )
        ''')
        conn_proxy.execute_without_result(f'''
            create table if not exists {table_name} (
                a varchar,
                b text,
                c float,
                d varchar,
                PRIMARY KEY (a, b)
            )
        ''')

        conn_normal.save_data(table_name, [
            {'a': 'a1', 'b': 'b1', 'c': None, 'd': None}
        ])
        data_list = conn_normal.run_sql_return_plain_json(f'''
            select * from {table_name}
        ''')
        self.assertEqual(len(data_list), 1)
        conn_normal.drop_table(table_name)

        conn_proxy.save_data(table_name, [
            {'a': 'a1', 'b': 'b1', 'c': None, 'd': None}
        ])
        conn_proxy.save_data(table_name, [
            {'a': 'a1', 'b': 'b1', 'c': 1.0, 'd': 'd1'}
        ], show_log=False)
        data_list = conn_proxy.run_sql_return_plain_json(f'''
            select * from {table_name}
        ''')
        self.assertEqual(len(data_list), 1)
        conn_proxy.drop_table(table_name)
        pass

    def test_process_geometry(self):
        table_name = 'test_algtmp_geometry_info'

        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        postgre = PostgreConnector(cfg.get('connector.postgres'))

        postgre.create_table([
            {
                'column_name': 'a',
                'data_type': 'geometry'
            }
        ], table_name, force=True)
        postgre.execute_sql(f'''
            insert into {table_name} values (ST_GeomFromText('Point(1 1)'))
        ''')
        data_list = postgre.run_sql_return_plain_json(f'select * from {table_name}')
        postgre.save_data(table_name, data_list)
        postgre.drop_table(table_name)
        pass

    def test_save_data_miss_column(self):
        table_name = 'test_algtmp_save_missing_column'

        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))

        conn_normal = PostgreConnector(cfg.get('connector.postgres_uri'))
        conn_normal.execute_without_result(f'''
            create table if not exists {table_name} (
                a varchar,
                b text,
                c varchar,
                d varchar,
                PRIMARY KEY (a, b)
            )
        ''')
        conn_normal.save_data(table_name, [
            {'a': 'a', 'b': 'b', 'c': 'c', 'd': 'd'},
            {'a': 'a0', 'b': 'b0'},
            {'a': 'a1', 'b': 'b1', 'c': 'c1'},
        ])
        conn_normal.drop_table(table_name)
        pass

    def test_save_geometry_table_with_schema(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))
        schema_name = 'test_schema'
        table_name = f'{schema_name}.algtmp_test_geom_table_with_schema'
        conn_normal = PostgreConnector(cfg.get('connector.postgres_uri'))
        conn_normal.run_sql_without_result(f'''
            create schema if not exists {schema_name}
        ''')

        data_list = [
            {
                'lnglat_seq': '1;1'
            }
        ]
        conn_normal.save_geometry_table(table_name, data_list)

        conn_normal.drop_table(table_name)
        conn_normal.run_sql_without_result(f'''
            drop schema if exists {schema_name}
        ''')
        pass

    def test_save_duplicate_geometry_table(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))

        table_name = 'algtmp_test_duplicate_geometry_table'
        conn_normal = PostgreConnector(cfg.get('connector.postgres_uri'))

        data_list = [
            {
                'lnglat_seq': '1;1'
            }
        ]
        conn_normal.save_geometry_table(table_name, data_list)
        conn_normal.save_geometry_table(table_name, data_list)

        conn_normal.drop_table(table_name)
        pass

    def test_ignore_cols(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))

        table_name = 'algtmp_test_ignore_cols'
        conn_normal = PostgreConnector(cfg.get('connector.postgres_uri'))
        data_list = [
            {
                'a': '1;1',
                'ignored': '2'
            }
        ]
        conn_normal.create_table_and_save_data(table_name, data_list, ignore_cols=['ignored'])
        d = conn_normal.run_sql_return_plain_json(f'''
            select * from {table_name}
        ''')
        self.assertTrue('ignored' not in d[0])
        conn_normal.drop_table(table_name)

        from bigdata_util.connector.postgre_async import PostgreAsyncConnector
        postgre = PostgreAsyncConnector(cfg.get('connector.postgres_uri'))
        postgre.drop_table(table_name)
        postgre.create_table([
            {
                'column_name': 'a',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            },
            {
                'column_name': 'ignored',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            },
        ], table_name, ignore_cols=['ignored'])
        postgre.save_data(table_name, [
            {'a': '1', 'ignored': '2'},
        ], show_log=True, batch_size=1, ignore_cols=['ignored'])
        d = postgre.run_sql_return_plain_json(f'''
            select * from {table_name}
        ''')
        self.assertTrue('ignored' not in d[0])
        postgre.drop_table(table_name)
        pass

    def test_pick_cols(self):
        cfg = BaseConfig(config_file_path=get_absolute_path(__file__, '../config.conf'))

        table_name = 'algtmp_test_ignore_cols'
        conn_normal = PostgreConnector(cfg.get('connector.postgres_uri'))
        data_list = [
            {
                'a': '1;1',
                'ignored': '2'
            }
        ]
        conn_normal.create_table_and_save_data(table_name, data_list, ignore_cols=[], pick_cols=['a'])
        d = conn_normal.run_sql_return_plain_json(f'''
            select * from {table_name}
        ''')
        self.assertTrue('ignored' not in d[0])
        conn_normal.drop_table(table_name)

        from bigdata_util.connector.postgre_async import PostgreAsyncConnector
        postgre = PostgreAsyncConnector(cfg.get('connector.postgres_uri'))
        postgre.drop_table(table_name)
        postgre.create_table([
            {
                'column_name': 'a',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            },
            {
                'column_name': 'ignored',
                'data_type': 'varchar',
                'is_nullable': True,
                'is_pk': True
            },
        ], table_name, ignore_cols=[], pick_cols=['a'])
        postgre.save_data(table_name, [
            {'a': '1', 'ignored': '2'},
        ], show_log=True, batch_size=1, ignore_cols=[], pick_cols=['a'])
        d = postgre.run_sql_return_plain_json(f'''
            select * from {table_name}
        ''')
        self.assertTrue('ignored' not in d[0])
        postgre.drop_table(table_name)
        pass


if __name__ == '__main__':
    unittest.main()
    pass
